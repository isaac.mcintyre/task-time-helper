
import { mount, shallow } from 'enzyme';
import React from 'react';
import Timer from '../components/Timer';
import TimerButton from '../components/TimerButton';

describe('Timer', () => {
  test('Button should be generated with correct data', () => {
    const handleSpy = jest.fn();
    // const timer = mount(<Timer />)
    const wrapper = shallow(<TimerButton handleSetTimerValue={handleSpy} timerInput={{time: 10, id: 5}} />)
    wrapper.find('button').first().simulate('click');
    expect(handleSpy.mock.calls.length).toBe(1);
    expect(handleSpy.mock.calls[0][0]).toBe(10);

  });

  test('should render', () => {
    const Wrapper = shallow(<Timer />);
    expect(Wrapper).toMatchSnapshot()
  })

});