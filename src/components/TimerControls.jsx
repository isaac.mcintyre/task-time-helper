import React, { Component } from "react";
class TimerControls extends Component {
  render() {
    return (
      <div className="timer-controls">
        <div>
          <button>Play</button>
        </div>
        <div>
          <button>Pause</button>
        </div>
        <div>
          <button>Stop</button>
        </div>
      </div>
    );
  }
}

export default TimerControls;
