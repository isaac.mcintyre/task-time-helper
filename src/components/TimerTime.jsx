import React from "react";
const TimerTime = (props) => {
  return (
    <div className="countdown">
      <h1 className="countdown">{props.minutes}:{props.seconds}</h1>
    </div>
  );
}

export default TimerTime;