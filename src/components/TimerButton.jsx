import React from 'react';

const TimerButton = (props) => (
  <div className="timer-button">
    <button onClick={ () => { props.handleSetTimerValue(props.timerInput.time);} }>{props.timerInput.id}</button>
  </div>
)
export default TimerButton;