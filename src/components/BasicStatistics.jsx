import React from 'react';
import TimeWorked from "./TimeWorked";

function BasicStatistics(props) {
  const timedSessionComponents = [];
  props.timerSessions.forEach(timerSession => {timedSessionComponents.push(<TimeWorked timerSession={timerSession} key={timerSession.id}/>);})
  return (
    <div className="basic-statistics">
      <h2>Here is where the statistics to your work are.</h2>
      <p>Number of 25 minute sessions left: {props.sessionsNeeded}</p>
      {timedSessionComponents}
    </div>
  );
}

export default BasicStatistics