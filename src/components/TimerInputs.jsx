import React, { Component } from "react";
import TimerButton from "./TimerButton";

class TimerInputs extends Component {
  render() {
    const workTimerButtons = [];
    const breakTimerButtons = [];
    this.props.timerInputs.forEach(timerInput => {
      if (timerInput.category === "work") {
        workTimerButtons.push(<TimerButton timerInput={timerInput} />);
      } else if (timerInput.category === "break") {
        breakTimerButtons.push(<TimerButton timerInput={timerInput} />);
      }
    });

    return (
      <div>
        <div>
          <h3>Work</h3>
          <div className="Hello There">{workTimerButtons}</div>
        </div>
        <div>
          <h3>Break</h3>
          <div>{breakTimerButtons}</div>
        </div>
      </div>
    );
  }
}

export default TimerInputs;
