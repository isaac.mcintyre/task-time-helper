import React from "react";
import "./Header.css";

function Header(props) {
  return (
    <div className="header">
      <img
        src="http://www.logospng.com/images/11/old-round-clock-8902-free-vectors-logos-icons-and-photos-11853.png"
        width="5%"
        alt="Temporary logo"
      />
      <h1>Task Time Helper</h1>
    </div>
  );
}

export default Header;
