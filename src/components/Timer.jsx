import React, { Component } from "react";
import TimerControls from "./TimerControls";
import TimerButtons from "./TimerButtons";
import TimerTime from "./TimerTime";

const TIMERINPUTS = [
  { category: "work", time: 25, id: "Pomodoro" },
  { category: "work", time: 10, id: "Task" },
  { category: "work", time: 1, id: "Minute" },
  { category: "break", time: 5, id: "Short" },
  { category: "break", time: 10, id: "Long" },
  { category: "break", time: 25, id: "Nap" }
];

class Timer extends Component {
  constructor(props) {
    super(props);
    this.handleSetTimerValue = this.handleSetTimerValue.bind(this);
    this.state = {
      seconds: 0,
      minutes: 25,
      startTimerValue: null, // to log how long spent studying in this session
      totalSessionTime: 0
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState((state) => ({ totalSessionTime: state.totalSessionTime+1 }));

      
    if (this.state.seconds === 0 && this.state.minutes > 0) {
      this.setState((state) => ({
        seconds: 59,
        minutes: this.state.minutes-1
      }));
    } else if (this.state.seconds === 0 && this.state.minutes === 0) {
      clearInterval(this.timerID); //disable this in future to stop ticking with user confirmation

      alert('Finish!')
    } else {
      this.setState({
        seconds: this.state.seconds-1
      })
    }
  }

  handleSetTimerValue(value) {
    this.setState({
      seconds: 0,
      minutes: value,
      timerStartValue: value
    });
  }

  render() {
    return (
        <div className="timer-components">
          <TimerTime minutes={this.state.minutes} seconds={this.state.seconds} />
          <TimerControls />
          <TimerButtons timerInputs={TIMERINPUTS} minutes={this.state.minutes} handleSetTimerValue={this.handleSetTimerValue} />
        </div>
    );
  }
}

export default Timer;
