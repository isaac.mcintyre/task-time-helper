import React, { Component } from "react";
import TimerButton from "./TimerButton";
import "./TimerButtons.css";

class TimerInputs extends Component {
  render() {
    const workTimerButtons = [];
    const breakTimerButtons = [];
    const handleSetTimerValue = this.props.handleSetTimerValue;
    this.props.timerInputs.forEach(timerInput => {
      if (timerInput.category === "work") {
        workTimerButtons.push(<TimerButton timerInput={timerInput} key={timerInput.id} handleSetTimerValue={handleSetTimerValue} />);
      } else if (timerInput.category === "break") {
        breakTimerButtons.push(<TimerButton timerInput={timerInput} key={timerInput.id} handleSetTimerValue={handleSetTimerValue} />);
      }
    });

    return (
      <React.Fragment>
        <div className="timer-buttons">
          <div className="timer-buttons-list"><h3>Work</h3>{workTimerButtons}</div>
          <div className="timer-buttons-list"><h3>Break</h3>{breakTimerButtons}</div>
        </div>
      </React.Fragment>
    );
  }
}

export default TimerInputs;
