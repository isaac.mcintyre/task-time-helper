import React from 'react';

const TimeWorked = (props) => (
  <div className="work-session">
    <p>{props.timerSession.time}</p>
  </div>
)

export default TimeWorked;