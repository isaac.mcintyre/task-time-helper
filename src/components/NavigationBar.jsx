import React, { Component } from "react";
import "./NavigationBar.css";

class NavigationBar extends Component {
  render() {
    return (
      <div>
        <a href="#timer">Timer</a>
        <a href="#tasks">Tasks</a>
        <a href="#custom">Customisation</a>
      </div>
    );
  }
}

export default NavigationBar;
