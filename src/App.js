import React, { Component } from "react";
import Header from "./components/Header";
import Timer from "./components/Timer";
import BasicStatistics from "./components/BasicStatistics";

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      timerSessions: [{ id: new Date(), time: 25, category: "work" }]
    }
    this.handleAddTimerValue = this.handleAddTimerValue.bind(this);
  }

  handleAddTimerValue(id, time, category) {
    console.log(this.state.timerSessions);
    this.setState((state) => ({ timerSessions: [...state.timerSessions, {id: id, time: time, category: category}] }));
  }

  render() {
    return (
      <div className="body">
        <Header />
        <Timer handleAddTimerValue={this.handleAddTimerValue} />
        <BasicStatistics timerSessions={this.state.timerSessions} sessionsNeeded={5} />
      </div>
    );
  }
}

export default App;
